import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kiwi/kiwi.dart' as kiwi;

class BaseBlocProvider<T extends Bloc<dynamic, dynamic>> extends BlocProvider<T> {
  BaseBlocProvider({
    Key key,
    Widget child,
    T bloc,
  }) : super(
      key: key,
      builder: (context) {
        return bloc ?? kiwi.Container().resolve<T>();
      },
      child: child);
}
