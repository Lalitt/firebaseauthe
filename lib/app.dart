import 'package:flutter/material.dart';
import 'bloc/bloc.dart';
import 'bloc/events.dart';
import 'bloc/states.dart';
import 'bloc/blocbuilder/base-bloc-builder.dart';
import 'bloc/blocbuilder/base-bloc-provider.dart';
import 'package:firebase_auth/firebase_auth.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PhoneSignInSectionState();
}

class _PhoneSignInSectionState extends State<App> {
  final TextEditingController _phoneNumber = TextEditingController();
  final TextEditingController _smsCode = TextEditingController();

  AuthBloc _authBloc;
  FirebaseUser user;
  bool editable = true;
  bool pressed = false;
  final _key = GlobalKey<FormState>();

  void initState() {
    super.initState();
    _authBloc = new AuthBloc();
    _authBloc.dispatch(OpenPage());
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Sign in'),
        ),
        body: BaseBlocProvider<AuthBloc>(
          bloc: _authBloc,
          child: Container(
            child: BaseBlocBuilder(
              bloc: _authBloc,
              builder: (BuildContext context, AuthState state) {
                return Scrollbar(
                    child: SingleChildScrollView(
                        child: authWidget(context, state)));
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget authWidget(context, state) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Form(
        key: _key,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: const Text('Pls watch logcat for details in studio'),
              padding: const EdgeInsets.all(16),
              alignment: Alignment.center,
            ),
            TextFormField(
              controller: _phoneNumber,
              keyboardType: TextInputType.phone,
              enabled: editable,
              validator: (String value) {
                if (value.isEmpty) {
                  return 'Phone number (+x xxx-xxx-xxxx)';
                }
                return null;
              },
              decoration: InputDecoration(
                  labelText: 'Phone number',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))),
            ),
            SizedBox(height: 50),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              alignment: Alignment.center,
              child: RaisedButton(
                  child: const Text('Send OTP'),
                  onPressed: () {
                    _authBloc
                        .dispatch(PressOnVerifyEvent(_phoneNumber.text));
                    setState(() {
                      pressed = true;
                    });
                  }
                  ),
            ),
            Container(
              child: verifCode(context, state),
            )
          ],
        ),
      ),
    );
  }
  verifCode(context, state) {
    if (pressed == false) {
      return null;
    } else if (pressed == true && _key.currentState.validate()) {
      return Container(
        child: Column(
          children: <Widget>[
            TextField(
              controller: _smsCode,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                  labelText: 'Verification code',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              alignment: Alignment.center,
              child: RaisedButton(
                onPressed: () {
                  _authBloc.dispatch(SubmitEvent(_smsCode.text));
                },
                child: const Text('Sign In'),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: fireb(user),
            )
          ],
        ),
      );
    }
  }

  fireb(FirebaseUser user) {
    if (user != null) {
      return Text('user logged in $user');
    }
  }
}
