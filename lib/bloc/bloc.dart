import '../bloc/blocbuilder/base-bloc.dart';
import 'events.dart';
import 'states.dart';
import 'package:firebaseauth/user-repository.dart';

class AuthBloc extends BaseBloc<AuthEvents, AuthState> {
  get initialState => Loading();

  final UserRepository _userRepository = UserRepository();

  AuthBloc();

  @override
  Stream<AuthState> mapEventToState(AuthEvents event) async* {
    if (event is OpenPage) {
      yield loading();
    }
    else if (event is PressOnVerifyEvent) {
      yield await otpSent(event.phoneNumber);
    } else if (event is OtpVerificationEvent) {
      yield  userVerified();
    } else if (event is ResendOtpEvent) {
      yield await otpSent(event.phoneNumber);
    } else if (event is SubmitEvent) {
      yield await loaded(event.smsCode);
    }
  }

  loading() {
    return Loading();
  }

  otpSent(phoneNoController) async {
    await _userRepository.phoneCodeToPhoneNumber(phoneNoController);
    return OtpSentState();
  }

  userVerified() {
    return UserVerifiedState();
  }

  loaded(smsController) async {
    await _userRepository.signInWithPhoneNumber(smsController);
    return LoadedState();
  }
}
