abstract class AuthState {}

class Loading extends AuthState {
}
class OtpSentState extends AuthState {}

class UserVerifiedState extends AuthState {
}

class LoadedState extends AuthState {}
