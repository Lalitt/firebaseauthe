import 'package:bloc/bloc.dart';

abstract class BaseBloc<Event, State> extends Bloc<Event, State> {
  @override
  void onError(Object error, StackTrace stacktrace) {
    super.onError(error, stacktrace);
    print(error);
    print(stacktrace);
  }
}
