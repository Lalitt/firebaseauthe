class AuthEvents {}

class OpenPage extends AuthEvents {}

class PressOnVerifyEvent extends AuthEvents {
  String phoneNumber;

  PressOnVerifyEvent(this.phoneNumber);
}

class OtpVerificationEvent extends AuthEvents {

}

class ResendOtpEvent extends AuthEvents {
  String phoneNumber;
  ResendOtpEvent(this.phoneNumber);
}

class SubmitEvent extends AuthEvents {
  String smsCode;
  SubmitEvent(this.smsCode);
}
