import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserRepository{
  String verificationID;


  Future<void> signInWithPhoneNumber(smsController) async {
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: verificationID,
      smsCode: smsController,
    );
    final FirebaseUser user =
        await FirebaseAuth.instance.signInWithCredential(credential);
    final FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();
    assert(user.uid == currentUser.uid);
    print('signed in with phone number successful: user -> $user');
  }

  Future<void> phoneCodeToPhoneNumber(phoneNoController) async {
    final PhoneVerificationCompleted verificationCompleted =
        (AuthCredential credential) {
    };

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      print(authException);
    };

    final PhoneCodeAutoRetrievalTimeout phoneCodeAutoRetrievalTimeout =
        (String verificationId) {
      verificationID = verificationId;
    };

    final PhoneCodeSent phoneCodeSent =
        (String verificatioNID, [int forceResendingToken]) {
      verificationID = verificatioNID;
      print('code sent');
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNoController,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: phoneCodeSent,
        codeAutoRetrievalTimeout: phoneCodeAutoRetrievalTimeout);
  }

}
