import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BaseBlocBuilder<E, S> extends StatelessWidget {
  final BlocWidgetBuilder<S> builder;
  final BlocWidgetListener<S> listener;
  final Bloc<E, S> bloc;

  final widgets = [];

  BaseBlocBuilder({
    Key key,
    this.builder,
    this.listener,
    @required this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    if (listener != null)
      return BlocListener<E, S>(
        bloc: bloc,
        listener: listener,
        child: getBuilder(bloc),
      );
    else {
      return getBuilder(bloc);
    }
  }

  getBuilder(Bloc<E, S> _bloc) {
    if (builder != null) {
      return BlocBuilder<E, S>(bloc: _bloc, builder: builder);
    } else
      return Container();
  }
}
